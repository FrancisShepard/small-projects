const list = document.querySelector('.list');
let buttons = document.querySelectorAll('.delete'),
    listItems = [];

const itemName = document.querySelector('#itemName'),
      addItem = document.querySelector('#addItem'),
      alert = document.querySelector('.alert');

buttons.forEach(addButtons);

function addButtons(button){
    button.addEventListener('click', function(){
        this.parentNode.remove();
        listItems = [];
        updateList();
        localStorage.setItem('item', JSON.stringify(listItems));
    });
}

addItem.addEventListener('click', function(a){
    a.preventDefault();
    alert.style.display = "none";
    if(itemName.value === ''){
        alert.style.display = "block";
        return;
    }
    list.innerHTML += `<li class="task">${itemName.value}<button type="button" class="delete"><i class="fa fa-trash"></button></li>`;
    buttons = document.querySelectorAll('.delete');
    buttons.forEach(addButtons);
    listItems.push(itemName.value);
    localStorage.setItem('item', JSON.stringify(listItems));
    itemName.value = '';
});

function getName(){
    if(localStorage.getItem('item') !== null){
        let listItems = JSON.parse(localStorage.getItem('item'));
        listItems.forEach(function(item){
            list.innerHTML += `<li class="task">${item}<button type="button" class="delete"><i class="fa fa-trash"></button></li>`;
        });
        
        buttons = document.querySelectorAll('.delete');
        buttons.forEach(addButtons);
    }
}

function updateList(){
    let liItems = list.querySelectorAll('.task');
    for(let i=0; i < liItems.length; i++){
        listItems.push(liItems[i].textContent);
    }
}

getName();
updateList();