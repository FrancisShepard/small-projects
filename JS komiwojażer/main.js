var cities = [];
var totalCities = 6;

var order = [];

var recordDistance;
var bestPath;

function setup(){
    createCanvas(400, 600);
    for(var i = 0; i < totalCities; i++){
        var vect = createVector(random(width), random(height/2));
        cities[i] = vect;
        order[i] = i;
    }
    
    var d = calcDistance(cities, order);
    recordDistance = d;
    bestPath = order.slice();
}

function draw(){
    background(0);
    fill(255);
    
    stroke(255, 0, 0);
    for(var i = 0; i < cities.length; i++){
        ellipse(cities[i].x, cities[i].y, 10, 10);
    }
    
    stroke(255, 255, 0);
    strokeWeight(4);
    noFill();
    beginShape();
    for(var i = 0; i < order.length; i++){
        var n = bestPath[i];
        vertex(cities[n].x, cities[n].y);
    }
    endShape();
    
    translate(0, height/2);
    
    stroke(255, 0, 0);
    for(var i = 0; i < cities.length; i++){
        ellipse(cities[i].x, cities[i].y, 10, 10);
    }
    
    stroke(255);
    strokeWeight(2);
    noFill();
    beginShape();
    for(var i = 0; i < order.length; i++){
        var n = order[i];
        vertex(cities[n].x, cities[n].y);
    }
    endShape();
    
    var d = calcDistance(cities, order);
    if (d<recordDistance){
        recordDistance = d;
        bestPath = order.slice();
    }
    
    nextOrder();
}

function swap(a, i, j){
    var temp = a[i];
    a[i] = a[j];
    a[j] = temp;
}

function calcDistance(points, order){
    var sum = 0;
    for(var i = 0; i < order.length - 1; i++){
        var cityA = points[order[i]];
        var cityB = points[order[i+1]];
        var d = dist(cityA.x, cityA.y, cityB.x, cityB.y);
        sum += d;
    }
    return sum;
}


function nextOrder(){
    var largestI = -1;
    for (var i = 0; i < order.length; i++){
        if (order[i] < order[i+1]){
            largestI = i;
        }
    }
    if (largestI == -1){
        noLoop();
    }
    
    var largestJ = -1;
    for (var j = 0; j < order.length; j++){
        if (order[largestI] < order[j]){
            largestJ = j;
        }
    }
    
    swap(order, largestI, largestJ);
    
    var endArray = order.splice(largestI+1)
    endArray.reverse();
    order = order.concat(endArray);
}