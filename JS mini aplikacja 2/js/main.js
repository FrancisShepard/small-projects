var val = document.getElementById('lbsInput');

document.getElementById('output').style.visibility = 'hidden';

val.addEventListener('input', function(e){
    document.getElementById('output').style.visibility = 'visible';
    let lbs = e.target.value;
    let grams = lbs/0.0022046;
    let kilo = lbs/2.2046;
    let oz = lbs*16;
    document.getElementById('gramsOutput').innerHTML = grams.toFixed(2);
    document.getElementById('kgOutput').innerHTML = kilo.toFixed(2);
    document.getElementById('ozOutput').innerHTML = oz.toFixed(2);
    
    if(val.value == ''){
        document.getElementById('output').style.visibility = 'hidden';
    }
});
